/* eslint-disable react/jsx-no-target-blank */
import React from 'react'
import {useIntl} from 'react-intl'
import {KTIcon} from '../../../../helpers'
import {SidebarMenuItemWithSub} from './SidebarMenuItemWithSub'
import {SidebarMenuItem} from './SidebarMenuItem'

const SidebarMenuMain = () => {
  const intl = useIntl()

  return (
      <>
          <SidebarMenuItem
              to='/dashboard'
              title={intl.formatMessage({id: 'MENU.DASHBOARD'})}
              fontIcon='bi-app-indicator'
          />
          <div className='menu-item'>
              <div className='menu-content pt-8 pb-2'>
                  <span className='menu-section text-muted text-uppercase fs-8 ls-1'>Members Information</span>
              </div>
          </div>
          <SidebarMenuItem to='/my-page' title='My page' fontIcon='bi-droplet-half text-primary'>
          </SidebarMenuItem>
      </>
  )
}

export {SidebarMenuMain}
